<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit981bd439dc49fae71d4d18b359f187a7
{
    public static $prefixLengthsPsr4 = array (
        'I' => 
        array (
            'Interfaces\\' => 11,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Interfaces\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Interfaces',
        ),
    );

    public static $classMap = array (
        'DevNullPubSubAdapter' => __DIR__ . '/../..' . '/Adapters/DevNullPubSubAdapter.php',
        'LocalPubSubAdapter' => __DIR__ . '/../..' . '/Adapters/LocalPubSubAdapter.php',
        'TestClass' => __DIR__ . '/../..' . '/MyClasses/TestClass.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit981bd439dc49fae71d4d18b359f187a7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit981bd439dc49fae71d4d18b359f187a7::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit981bd439dc49fae71d4d18b359f187a7::$classMap;

        }, null, ClassLoader::class);
    }
}
