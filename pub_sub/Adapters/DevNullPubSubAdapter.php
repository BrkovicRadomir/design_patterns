<?php


use Interfaces\PubSubAdapterInterface;

class DevNullPubSubAdapter implements PubSubAdapterInterface
{

    public function subscribe($channel, callable $hnadler){
      // This is null adapter so we don't do noting
    }


    public function publish($channel, $message){
      // This is null adapter so we don't do noting
    }

    public function publishBatch($channel, array $messages){
      // This is null adapter so we don't do noting
    }

}
