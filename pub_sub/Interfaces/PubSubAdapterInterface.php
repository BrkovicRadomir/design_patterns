<?php
namespace Interfaces;

interface PubSubAdapterInterface {

  /**
   * Subscribe a handler to a channel
   * @param string $channel
   * @param callable $handler
   */
  public function subscribe($channel, callable $handler);


  /**
  * Publish message to a channel
  * @param string $channel
  * @param mixed $message
  */
  public function publish($channel, $message);

  /**
  * Publish multiple messages to a channel
  * @param string $channel
  * @param array $messages
  */
  public function publishBatch($channel, array $messages);

}
