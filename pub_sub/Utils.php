<?php

namespace PubSub;

abstract class Utils
{

    public static function serializeMessage($message){
      return json_encode($message);
    }


    public static function unserializeMessagePayload($payload){

        $message = json_decode($payload, true);
        if(json_last_error() == JSON_ERROR_NONE){
          return $message;
        }

        return $payload;
    }

}
