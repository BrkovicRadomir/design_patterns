<?php

include_once 'Logger.php';
include_once 'ErrorHandler.php';
$logger = new Logger();

$errorHandler = new ErrorHandler($logger);


set_error_handler(function ($errorno, $errstr, $errorfile, $errorline) use($errorHandler){
   return $errorHandler->error($errorno, $errstr, $errorfile, $errorline);
});

set_exception_handler(function($exception) use($errorHandler){
   return $errorHandler->exception($exception);
});

if(1 > 5)
    echo "OK";
else
    throw new Exception("One is less than five ");