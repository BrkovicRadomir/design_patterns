<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/5/18
 * Time: 9:13 AM
 */

class ErrorHandler
{
    private $logger;


    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }


    public function error($errorno, $errstr, $errorfile, $errorline){
            $this->logger->log("Error :".$errstr);

            return true;
    }


    public function exception(Exception $exception){
        $this->logger->log("Exception ".$exception->getMessage());
        return true;
    }

}