<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/5/18
 * Time: 8:32 AM
 */

interface PaymentInterface
{

    public function processPayment();

}