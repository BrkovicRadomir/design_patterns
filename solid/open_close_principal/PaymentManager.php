<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/3/18
 * Time: 10:31 PM
 */

class PaymentManager
{

    protected $paypal;


    public function __construct(PaymentInterface $paypalIpn)
    {
        $this->paypal = $paypalIpn;
    }

    public function process(){
        $this->paypal->processPayment();
    }

}