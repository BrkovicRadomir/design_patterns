<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 5/5/18
 * Time: 8:39 AM
 */


class Driver
{

    protected $car;

    public function __construct(Car $car)
    {
        $this->car = $car;
    }



    public function go(){
        $this->car->drive();
    }


}