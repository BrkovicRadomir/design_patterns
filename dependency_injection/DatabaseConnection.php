<?php

namespace DI;

use DI\DatabaseConfiguration;

class DatabaseConnection
{

    /**
     * @var DatabaseConfiguration
     */
    private $configuration;

    public function __construct(DatabaseConfiguration $conf){
        $this->configuration = $conf;
    }


    /**
     * @return string
     */
    public function getDsn(): string{

        return sprintf('%s:%s@%s:%d',
            $this->configuration->getUsername(),
            $this->configuration->getPassword(),
            $this->configuration->getHost(),
            $this->configuration->getPort());
    }
}