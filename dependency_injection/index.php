<?php
require_once __DIR__ . '/vendor/autoload.php';

$conf = new DI\DatabaseConfiguration('localhost', 3306, 'root', '123');
$connect = new DI\DatabaseConnection($conf);

echo $connect->getDsn();
