<?php

include_once (__DIR__.'/Form.php');
include_once (__DIR__.'/InputElement.php');
include_once (__DIR__.'/TextElement.php');

$form = new Form();
$form->addElements(new TextElement('Email:'));
$form->addElements(new InputElement());

$embed = new Form();
$embed->addElements(new TextElement('Password: '));
$embed->addElements(new InputElement());
$form->addElements($embed);

echo $form->render();