<?php


include_once (__DIR__.'/RenderableInterface.php');


class InputElement implements RenderableInterface
{
    public function render(): string {
        return "<input type=\"text\" />";
    }
}