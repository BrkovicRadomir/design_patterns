<?php

 include_once (__DIR__.'/RenderableInterface.php');
class Form implements  RenderableInterface
{

    /**
     * @var RenderableInterface[]
     */

    private $elements;

    /**
     * @return string
     */
    public function render():string {
        $formCode = "<form>";

        foreach($this->elements as $element){
            $formCode .= $element->render();
        }

        $formCode .= "</form>";

        return $formCode;
    }

    /**
     * @param RenderableInterface $element
     */

    public function addElements(RenderableInterface $element){
        $this->elements[] = $element;
    }

}