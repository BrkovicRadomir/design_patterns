<?php

namespace Adapter;

require_once __DIR__."/vendor/autoload.php";


use PHPUnit\Framework\TestCase;


class AdapterTest extends TestCase
{

    public function testCanTurnPageOn(){
        $book = new BookClass();

        $book->open();
        $book->turnPage();

        $this->assertEquals(2, $book->getPage());
    }


    public function testCanTurnPageOnKindleLikeInANormalBook()
    {
        $kindle = new Kindle();
        $book = new EBookAdapter($kindle);

        $book->open();
        $book->turnPage();

        $this->assertEquals(2, $book->getPage());
    }
}