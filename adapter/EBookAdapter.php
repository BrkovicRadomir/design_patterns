<?php


namespace Adapter;

use Adapter\Interfaces\BookInterface;
use Adapter\Interfaces\EBookInterface;

class EBookAdapter implements BookInterface
{

    private $eBook;

    public function __construct(EBookInterface $eBook){
        $this->eBook = $eBook;
    }


    public function open(){
        $this->eBook->unlock();
    }

    public function turnPage(){
        $this->eBook->pressNext();
    }

    public function  getPage(){
      return  $this->eBook->getPage()[0];
    }

}