<?php


namespace Adapter;


use Adapter\Interfaces\EBookInterface;

class Kindle implements EBookInterface
{
    private  $page = 1;

    private  $totalPage = 100;

    public function pressNext(){
      $this->page++;
    }

    public function unlock(){}

    public function getPage(){
        return [$this->page, $this->totalPage];
    }

}