<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 7.8.17.
 * Time: 14.16
 */

namespace Adapter\Interfaces;


interface EBookInterface
{

    public function unlock();

    public function pressNext();

    public function getPage();
}