<?php

namespace Adapter\Interfaces;


interface BookInterface
{
    public function turnPage();

    public function open();

    public function getPage();

}