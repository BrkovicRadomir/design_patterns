<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 7.8.17.
 * Time: 14.12
 */

namespace Adapter;


use Adapter\Interfaces\BookInterface;

class BookClass implements BookInterface
{

    /**
     * @var int
     */
    private $page = 1;


    public function open(){
        $this->page = 1;
    }

    public function getPage(){
        return $this->page;
    }

    public function turnPage(){
        $this->page++;
    }
}