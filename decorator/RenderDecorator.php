<?php

include_once(__DIR__."/RenderableInterface.php");

abstract class RenderDecorator implements RenderableInterface
{
    /**
     * @var RenderableInterface
     */
    protected $wrapped;

    public function __construct(RenderableInterface $renderer){
        $this->wrapped = $renderer;
    }

}