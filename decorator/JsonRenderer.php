<?php

include_once (__DIR__."/RenderDecorator.php");

class JsonRenderer extends  RenderDecorator
{
    public function renderData():string {
        return json_encode($this->wrapped->renderData());
    }

}