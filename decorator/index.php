<?php

include_once(__DIR__."/Webservice.php");
include_once(__DIR__."/JsonRenderer.php");
include_once(__DIR__."/XmlRenderer.php");

$service = new Webservice("foobar");

$json = new JsonRenderer($service);
var_dump($json->renderData());

$xml = new XmlRenderer($service);
var_dump($xml->renderData());