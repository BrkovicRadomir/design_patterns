<?php
 include_once (__DIR__."/RenderableInterface.php");

class Webservice implements RenderableInterface
{
    /**
     * @var string
     */
    private $data;


    public function __construct(string $data){
        $this->data = $data;
    }


    public function renderData(): string {
        return $this->data;
    }
}