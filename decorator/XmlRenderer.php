<?php

include_once (__DIR__.'/RenderDecorator.php');

class XmlRenderer extends RenderDecorator
{
    public function renderData():string {
        $doc = new \DOMDocument();
        $data = $this->wrapped->renderData();
        $doc->appendChild($doc->createElement('content', $data));

        return $doc->saveXML();
    }

}