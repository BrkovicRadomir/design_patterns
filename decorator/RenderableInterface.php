<?php


interface RenderableInterface
{
    public function renderData(): string;
}