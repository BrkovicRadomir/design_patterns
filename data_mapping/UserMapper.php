<?php

//namespace DataMapping;

class UserMapper
{

    /**
     * @var StorageAdapter
     */

    private  $adapter;


    /**
     * @param StorageAdapter $adapter
     */
    public function __construct(StorageAdapter $adapter){
        $this->adapter = $adapter;
    }


    /**
     * @param $id
     * @return User
     */
    public function findById($id){
        $result = $this->adapter->find($id);

        if($result === null)
            throw new \InvalidArgumentException("User #$id not found");

        return $this->mapRowToUser($result);
    }


    /**
     * @param array $row
     * @return User
     */
    private function mapRowToUser(array $row): User
    {
        return User::fromState($row);
    }

}