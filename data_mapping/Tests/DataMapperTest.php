<?php


namespace DataMapping\Tests;

include_once('../StorageAdapter.php');
include_once('../User.php');
include_once('../UserMapper.php');

use DataMapping\StorageAdapter;
use DataMapping\User;
use DataMapping\UserMapper;
use PHPUnit\Framework\TestCase;

class DataMapperTest extends TestCase
{
    public function testCanMapUserFromStorage()
    {
        $storage = new StorageAdapter([1 => ['username' => 'pera_peric', 'email' => 'sdsds@ssa.com']]);
        $mapper = new UserMapper($storage);

        $user = $mapper->findById(1);

        $this->assertInstanceOf(User::class, $user);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testWillNotMapInvalidData()
    {
        $storage = new StorageAdapter([]);
        $mapper = new UserMapper($storage);

        $mapper->findById(1);
    }
}