<?php

include_once(__DIR__.'/HelloWorldService.php');
include_once(__DIR__.'/HtmlFormatter.php');
include_once(__DIR__.'/PlainTextFormatter.php');

$service = new HelloWorldService(new PlainTextFormatter());
var_dump($service->get());
echo "<br />";
$service->setImplementation(new HtmlFormatter());
var_dump($service->get());
