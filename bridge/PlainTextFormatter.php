<?php

include_once(__DIR__.'/FormatterInterface.php');

class PlainTextFormatter implements FormatterInterface
{
    public function format(string $text){
        return $text;
    }

}