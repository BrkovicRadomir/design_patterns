<?php

include_once(__DIR__.'/Service.php');
class HelloWorldService extends Service
{
    public function get(){
        return $this->implementation->format("Hello World");
    }
}