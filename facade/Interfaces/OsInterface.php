<?php
/**
 * Created by PhpStorm.
 * User: rade
 * Date: 8.7.17.
 * Time: 08.07
 */

namespace Facade\Interfaces;


interface OsInterface
{
    public function halt();

    public function getName(): string;

}