<?php

namespace Facade\Tests;


require_once "/var/www/html/design_patterns/facade/vendor/autoload.php";


use Facade\Facade;
use Facade\Interfaces\OsInterface;
use PHPUnit\Framework\TestCase;

class FacadeTest extends TestCase
{


    public function testComputerOn(){
        /**
         * @var OsInterface|PHPUnit_Framework_MockObject_MockObject $os
         */
        $os = $this->createMock('Facade\Interfaces\OsInterface');

        $os->method('getName')->will($this->returnValue("Linux"));

        $bios = $this->getMockBuilder('Facade\Interfaces\BiosInterface')
            ->setMethods(['launch', 'execute', 'waitForKeyPress'])
            ->disableAutoload()->getMock();

        $bios->expects($this->once())->method('launch')->with($os);

        $facade = new Facade($bios, $os);

        $facade->turnOn();

        $this->assertEquals('Linux', $os->getName());
    }

}