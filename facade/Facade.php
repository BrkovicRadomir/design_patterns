<?php


namespace Facade;


use Facade\Interfaces\BiosInterface;
use Facade\Interfaces\OsInterface;

class Facade
{

    /**
     * @var OsInterface
     */
    private $os;

    /**
     * @var BiosInterface
     */

    private $bios;


    public function __construct(BiosInterface $bios, OsInterface $os){
        $this->bios = $bios;
        $this->os = $os;
    }


    public function turnOn(){
        $this->bios->execute();
        $this->bios->waitForKeyPress();
        $this->bios->launch($this->os);
    }

    public function turnOff(){
        $this->os->halt();
        $this->bios->powerDown();
    }
}